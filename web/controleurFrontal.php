<?php

////////////////////
// Initialisation //
////////////////////
require_once __DIR__ . '/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\UrlHelper;
use Symfony\Component\Routing\Generator\UrlGenerator;
use TheFeed\Lib\Conteneur;


/////////////
// Routage //
/////////////


TheFeed\Controleur\RouteurURL::traiterRequete();
